#include "I2C.h"

/* CONSTRUCTOR */ 
/***************/
I2C::I2C(int8_t clockPin, int8_t dataPin, int8_t address7bit)
{
   init(clockPin, dataPin, address7bit);
}


/* SETUP PINOUT FOR I2C COMMUNICATION */
/**************************************/
void I2C::init(int8_t clockPin, int8_t dataPin, int8_t address7bit)
{
   this->clockPin = clockPin;
   this->dataPin = dataPin;
   ModuleAddress.read = (address7bit << 1) | 0x01;
   ModuleAddress.write = (address7bit << 1);
}


/* INDIVIDUAL BUS LINE TOGGLE */
/******************************/
void I2C::driveLine(BusLine busLine, bool state)
{
   int8_t pin = (busLine == Clock) ? clockPin : dataPin;
   
   switch(state)
   {
   case HIGH:
      {
         pinMode(pin, INPUT);
         break;
      }
   case LOW:
      {
         pinMode(pin, OUTPUT);
         break;
      }
   }
   if (busLine == Clock) delayMicroseconds(5); // 100kHz
}


/* READ BIT FROM Data LINE */
/***************************/
bool I2C::readBit()
{
   driveLine(Clock, HIGH);
   bool inBit = digitalRead(dataPin);
   driveLine(Clock, LOW);
   return inBit;
}


/* WRITE BIT TO Data LINE */
/**************************/
void I2C::writeBit(bool outBit)
{
   driveLine(Data, outBit);
   driveLine(Clock, HIGH);
   driveLine(Clock, LOW);
}


/* START FRAME COMMAND */
/***********************/
void I2C::start()
{
   driveLine(Data, LOW);
   driveLine(Clock, LOW);
}


/* STOP FRAME COMMAND */
/**********************/
void I2C::stop()
{
   driveLine(Clock, HIGH);
   driveLine(Data, HIGH);
}


/* WRITE A SINGLE BYTE AND RETURN SLAVE ACKNOWLEDGE BIT (0 = SUCCESS) */
/**********************************************************************/
void I2C::writeByte(uint8_t outByte)
{
   for (int8_t i = 7; i >= 0; i--)
   {
      writeBit((outByte >> i) & 1);
   }
   
   // Request slave acknowledge bit
   ackSlaveFail |= readBit();
}


/* READ A SINGLE BYTE */
/***********************/
uint8_t I2C::readByte()
{
   uint8_t inByte = 0;
   for (int8_t i = 7; i >= 0; i--)
   {
      inByte |= (uint8_t)readBit() << i;
   }
   writeBit(1); // NACKM
   return inByte;
}


/* WRITE FRAME - SINGLE BYTE */
/*****************************/
void I2C::write(uint8_t writeReg)
{
   uint8_t reg[] = {writeReg};
   write(reg, 1);
}


/* WRITE FRAME - MULTI-BYTE */
/****************************/
void I2C::write(uint8_t writeFrame[], uint8_t writeByteQty)
{
   do
   {
      ackSlaveFail = false;
      start();
      writeByte(ModuleAddress.write);
      for (int8_t index = 0; index < writeByteQty; index++)
      {
         writeByte(writeFrame[index]);
      }
      stop();
   } while (ackSlaveFail);
}


/* READ FRAME */
/**************/
uint32_t I2C::read(uint8_t QtyOfBytesToRead)
{
   uint32_t readValue = 0;
   ackSlaveFail = false;
   while (QtyOfBytesToRead--)
   {
      start();
      writeByte(ModuleAddress.read);
      readValue <<= 8;
      readValue |= readByte();
      stop();
   };
   return readValue;
}