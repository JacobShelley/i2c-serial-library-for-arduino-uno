# This is a software-based I2C serial library #
## For Arduino Uno ##

* * *

### INITIALIZING ###

Initializing can be made through the constructor, or later using the init() method.  Initializing after object creation is useful in the case of creating a global instance of I2C, where pinout must be passed through via a scoped method.

EXAMPLE:

``` {.cpp}
int8_t clockPin = 7;
int8_t dataPin = 7;
uint8_t sensorAddress = 0x5E; // Accepts 7-bit addresses. 
```

AND EITHER
``` {.cpp}
I2C Sensor(clockPin, dataPin, sensorAddress);
```
OR
``` {.cpp}
I2C Sensor;
. . .
I2C.init(clockPin, dataPin, sensorAddress);
```
* * *

### WRITING DATA ###

Writing I2C data can be composed of a single byte, or an array of bytes.  The stop/start/ack bits required in the frame are handled internally.

EXAMPLE:

``` {.cpp}
uint8_t outByte = 0x1A;
Sensor.write(outByte);
```
OR:
``` {.cpp} 
uint8_t outBytes[] = {0x1A, 0x1B, 0x1C};
int8_t outBytesQty = 3;
Sensor.write(outBytes, outBytesQty);
```

### READING DATA ###

The quantity of desired bytes must be passed in as an argument to read data from I2C.

EXAMPLE:

~~~ {.cpp}
uint8_t OneByteData = Sensor.read(); // Default parameter is 1.
uint16_t TwoByteData = Sensor.read(2);
uint32_t ThreeByteData = Sensor.read(3);
uint32_t FourByteData = Sensor.read(4); // 4 bytes max.
~~~